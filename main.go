package unzip_api

import (
    "encoding/json"
    "io/ioutil"
    "net/http"
    "os"
)

func Handler(w http.ResponseWriter, r *http.Request) {
    zipFile, _ := ioutil.ReadAll(r.Body)
    unzipper := NewUnzipper()
    f, _ := ioutil.TempFile("", "unzipper_")
    defer os.Remove(f.Name())
    ioutil.WriteFile(f.Name(), zipFile, os.FileMode(0644))
    err := unzipper.Unzip(f.Name())
    if err != nil {
        w.WriteHeader(500)
        return
    }

    js, _ := json.MarshalIndent(unzipper, "", "    ")

    w.Header().Set("Content-Type", "application/json")
    w.Write(js)
}
