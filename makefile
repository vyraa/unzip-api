.PHONY: gomodgen deploy delete

gomodgen:
	GO111MODULE=on go mod init

deploy:
	gcloud functions deploy unzipapi --project entapps-integrations --entry-point Handler --runtime go113 --trigger-http

delete:
	gcloud functions delete unzipapi --project entapps-integrations --entry-point Handler --runtime go113 --trigger-http
