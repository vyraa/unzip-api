package unzip_api

import (
    "archive/zip"
    "io/ioutil"
    "log"
    "strings"
)

type UnzippedFile struct {
    Name string				`json:"name"`
    Contents string			`json:"contents"`
}

type Unzipper struct {
    Files []UnzippedFile	`json:"files"`
    Count int				`json:"count"`
}

type IUnzipper interface {
    Unzip(filename string) Unzipper
}

func NewUnzipper() *Unzipper {
    unzipper := new(Unzipper)

    unzipper.Files = []UnzippedFile{}
    unzipper.Count = 0

    return unzipper
}

func (uz *Unzipper) Unzip(filename string) error {
    r, err := zip.OpenReader(filename)
    if err != nil {
        log.Fatalln(err)
    }

    defer r.Close()

    for _, f := range r.File {
        if strings.Contains(f.Name, "__MACOSX") {
            continue
        }

        fileReader, _ := f.Open()

        contents, _ := ioutil.ReadAll(fileReader)

        uz.Files = append(uz.Files, UnzippedFile{
            Name: f.Name,
            Contents: string(contents),
        })

        uz.Count++
    }

    return nil
}
